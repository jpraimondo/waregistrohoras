﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WARegistroHoras.Shared
{
    public class ViewForm
    {
        public int Id { get; set; }
        public string Month { get; set; } = string.Empty;
        public string EmployeeName { get; set; } = string.Empty;
        public DateTime CreationDate { get; set; } = DateTime.Now;
        public DateTime SentDate { get; set;}= DateTime.MinValue;
        public bool Sent { get; set; } = false;
        public DateTime ConfirmDate { get; set; } = DateTime.MinValue;
        public bool Confirm { get; set; }= false;
        public DateTime ProcesedDate { get; set; } = DateTime.MinValue;
        public bool Procesed { get; set; }=false;

    }
}
